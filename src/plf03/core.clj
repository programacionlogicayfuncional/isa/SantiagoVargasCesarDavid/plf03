(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))
        
        z (comp f g h)]
    
    (z 10)))

(defn función-comp-2
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))

        z (comp f g h)]

    (z 8)))

(defn función-comp-3
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))
        
        z (comp f g h)]
    
    (z 78)))

(defn función-comp-4
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))

        z (comp f g h)]

    (z 100)))

(defn función-comp-5
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))

        z (comp f g h)]

    (z 165)))

(defn función-comp-6
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))

        z (comp f g h)]

    (z 23)))

(defn función-comp-7
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))
        
        z (comp f g h)]
    
    (z 1646)))

(defn función-comp-8
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))

        z (comp f g h)]

    (z 1123)))

(defn función-comp-9
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))
        
        z (comp f g h)]
    
    (z 1)))

(defn función-comp-10
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))

        z (comp f g h)]

    (z 19)))

(defn función-comp-11
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))

        z (comp f g h)]

    (z 17)))

(defn función-comp-12
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))

        z (comp f g h)]

    (z 1111)))

(defn función-comp-13
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))

        z (comp f g h)]

    (z 1143)))

(defn función-comp-14
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))

        z (comp f g h)]

    (z 187)))

(defn función-comp-15
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))
        
        z (comp f g h)]
    
    (z 187)))

(defn función-comp-16
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))

        z (comp f g h)]

    (z 198)))

(defn función-comp-17
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))
        
        z (comp f g h)]
    
    (z 1112)))

(defn función-comp-18
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))
        
        z (comp f g h)]
    
    (z 198)))

(defn función-comp-19
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))
        
        z (comp f g h)]
    
    (z 1432)))

(defn función-comp-20
  []
  (let [f (fn [xs] inc xs)
        g (fn [x] (* 3 x))
        h (fn [x] (* 2 x))

        z (comp f g h)]

    (z 1450)))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)


(defn función-complement-1
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 10)))

(defn función-complement-2
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 1)))

(defn función-complement-3
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 445)))

(defn función-complement-4
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 567)))

(defn función-complement-5
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 986)))

(defn función-complement-6
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 902)))

(defn función-complement-7
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 562)))

(defn función-complement-8
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 561)))

(defn función-complement-9
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 123)))

(defn función-complement-10
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 4000)))

(defn función-complement-11
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 122)))

(defn función-complement-12
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 49)))

(defn función-complement-13
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 91)))

(defn función-complement-14
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 954)))

(defn función-complement-15
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 9000)))

(defn función-complement-16
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 4431)))

(defn función-complement-17
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 1234)))

(defn función-complement-18
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 4311)))

(defn función-complement-19
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 9831)))

(defn función-complement-20
  []
  (let [f (fn [xs] (even? xs))
        z (complement f)]

    (z 5242)))

(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)


(defn función-constantly-1
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 2 3})))

(defn función-constantly-2
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 34 2 4 3})))

(defn función-constantly-3
  []
  (let [f [true]
        z (constantly f)]

    (z #{"dsds" 2 3})))

(defn función-constantly-4
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 "dfd" 3})))

(defn función-constantly-5
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 2 "d" "cfd"})))

(defn función-constantly-6
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 2 true})))

(defn función-constantly-7
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 2 false "fff"})))

(defn función-constantly-8
  []
  (let [f [true]
        z (constantly f)]

    (z #{true false})))

(defn función-constantly-9
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 2 "ds" false})))

(defn función-constantly-10
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 2 \b})))

(defn función-constantly-11
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 \a \b 3})))

(defn función-constantly-12
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 2 3 \c "ddd"})))

(defn función-constantly-13
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 2 3 "d"})))

(defn función-constantly-14
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 \a 2 "dd" 3})))

(defn función-constantly-15
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 2 3 false})))

(defn función-constantly-16
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 2 3 4 5})))

(defn función-constantly-17
  []
  (let [f [true]
        z (constantly f)]

    (z #{"d" "t" "r"})))

(defn función-constantly-18
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 true 2 false 3})))

(defn función-constantly-19
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 "r "2 false 3})))

(defn función-constantly-20
  []
  (let [f [true]
        z (constantly f)]

    (z #{1 \a 2 \x 3})))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)


(defn función-every-pred-1
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z false)))

(defn función-every-pred-2
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z true)))

(defn función-every-pred-3
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z 1)))

(defn función-every-pred-4
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z \b)))

(defn función-every-pred-5
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z 123)))

(defn función-every-pred-6
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z "hola")))

(defn función-every-pred-7
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z [1 2])))

(defn función-every-pred-8
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z [false true])))

(defn función-every-pred-9
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z "d")))

(defn función-every-pred-10
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z 1000)))

(defn función-every-pred-11
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z \a)))

(defn función-every-pred-12
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z [\a])))

(defn función-every-pred-13
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z [1 2])))

(defn función-every-pred-14
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z [1 2 3])))

(defn función-every-pred-15
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z [true true])))

(defn función-every-pred-16
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z "bye")))

(defn función-every-pred-17
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z 123)))

(defn función-every-pred-18
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z "ok")))

(defn función-every-pred-19
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z 12321)))

(defn función-every-pred-20
  []
  (let [f (fn [xs] (constantly xs))

        z (every-pred f)]

    (z \x)))

(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)


(defn función-fnil-1
  []
  (let [f (fn [xs] (nil? xs))
        g (fn[x] (x))

        z (fnil f g)]

    (z 10)))

(defn función-fnil-2
  []
  (let [f (fn [xs] (nil? xs))
        g (fn [x] (x))

        z (fnil f g)]

    (z true)))

(defn función-fnil-3
  []
  (let [f (fn [xs] (nil? xs))
        g (fn [x] (x))

        z (fnil f g)]

    (z 123)))

(defn función-fnil-4
  []
  (let [f (fn [xs] (nil? xs))
        g (fn [x] (x))

        z (fnil f g)]

    (z \b)))

(defn función-fnil-5
  []
  (let [f (fn [xs] (nil? xs))
        g (fn [x] (x))

        z (fnil f g)]

    (z "hola")))

(defn función-fnil-6
  []
  (let [f (fn [xs] (nil? xs))
        g (fn[x] (x))

        z (fnil f g)]

    (z [32 2])))

(defn función-fnil-7
  []
  (let [f (fn [xs] (nil? xs))
        g (fn [x] (x))

        z (fnil f g)]

    (z \a)))

(defn función-fnil-8
  []
  (let [f (fn [xs] (nil? xs))
        g (fn[x] (x))

        z (fnil f g)]

    (z 143)))

(defn función-fnil-9
  []
  (let [f (fn [xs] (nil? xs))
        g (fn [x] (x))

        z (fnil f g)]

    (z 1232)))

(defn función-fnil-10
  []
  (let [f (fn [xs] (nil? xs))
        g (fn[x] (x))

        z (fnil f g)]

    (z 1323)))

(defn función-fnil-11
  []
  (let [f (fn [xs] (nil? xs))
        g (fn [x] (x))

        z (fnil f g)]

    (z "bye")))

(defn función-fnil-12
  []
  (let [f (fn [xs] (nil? xs))
        g (fn [x] (x))

        z (fnil f g)]

    (z \x)))

(defn función-fnil-13
  []
  (let [f (fn [xs] (nil? xs))
        g (fn[x] (x))

        z (fnil f g)]

    (z [2 3])))

(defn función-fnil-14
  []
  (let [f (fn [xs] (nil? xs))
        g (fn[x] (x))

        z (fnil f g)]

    (z [true])))

(defn función-fnil-15
  []
  (let [f (fn [xs] (nil? xs))
        g (fn [x] (x))

        z (fnil f g)]

    (z "false")))

(defn función-fnil-16
  []
  (let [f (fn [xs] (nil? xs))
        g (fn[x] (x))

        z (fnil f g)]

    (z 13232)))

(defn función-fnil-17
  []
  (let [f (fn [xs] (nil? xs))
        g (fn[x] (x))

        z (fnil f g)]

    (z -1)))

(defn función-fnil-18
  []
  (let [f (fn [xs] (nil? xs))
        g (fn[x] (x))

        z (fnil f g)]

    (z "e")))

(defn función-fnil-19
  []
  (let [f (fn [xs] (nil? xs))
        g (fn[x] (x))

        z (fnil f g)]

    (z 103)))

(defn función-fnil-20
  []
  (let [f (fn [xs] (nil? xs))
        g (fn [x] (x))

        z (fnil f g)]

    (z [false true])))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)


(defn función-juxt-1
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a 1 :b 2})))

(defn función-juxt-2 
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a 2 :b 10})))

(defn función-juxt-3
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a "hola" :b 2})))

(defn función-juxt-4
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

(z {:a \a :b 32})))

(defn función-juxt-5
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

(z {:a \b :b \c})))

(defn función-juxt-6
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a "eee" :b 23})))

(defn función-juxt-7
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a 21 :b 245})))

(defn función-juxt-8
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a "fff" :b "rrr"})))

(defn función-juxt-9
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

(z {:a -1 :b \x})))

(defn función-juxt-10
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a 121 :b 222})))

(defn función-juxt-11
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a -1 :b -2})))

(defn función-juxt-12
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a "eee" :b true})))

(defn función-juxt-13
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a true :b false})))

(defn función-juxt-14
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a \b :b \p})))

(defn función-juxt-15
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a 0 :b 232})))

(defn función-juxt-16
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a "rrd" :b true})))

(defn función-juxt-17
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a (* 2 3) :b 43})))

(defn función-juxt-18
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a (+ 1 2) :b "hola"})))

(defn función-juxt-19
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a 132 :b 78})))

(defn función-juxt-20
  []
  (let [f (fn [xs] :a xs)
        g (fn [x] :b x)

        z (juxt f g)]

    (z {:a 1 :b (< 1 2)})))

(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)


(defn función-partial-1
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 2)))

(defn función-partial-2
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 92)))

(defn función-partial-3
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 123)))

(defn función-partial-4
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z -32)))

(defn función-partial-5
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 8/3)))

(defn función-partial-6
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 7)))

(defn función-partial-7
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 192)))

(defn función-partial-8
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 595)))

(defn función-partial-9
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 3933)))

(defn función-partial-10
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 244943)))

(defn función-partial-11
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 243432)))

(defn función-partial-12
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 222)))

(defn función-partial-13
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 6/6)))

(defn función-partial-14
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 8/7)))

(defn función-partial-15
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 9000)))

(defn función-partial-16
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 243)))

(defn función-partial-17
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 433)))

(defn función-partial-18
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z 1211)))

(defn función-partial-19
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

    (z -9/6)))

(defn función-partial-20
  []
  (let [f (fn [xs] (* xs xs))

        z (partial f)]

   (z 43)))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)


(defn función-some-fn-1
  []
  (let [f (fn[x] (true? x))
        
        z (some-fn f)]

    (z 8)))

(defn función-some-fn-2
  []
  (let [f (fn[x] (true? x))
        
        z (some-fn f)]

    (z 843)))

(defn función-some-fn-3
  []
  (let [f (fn[x] (true? x))
        
        z (some-fn f)]

    (z false)))

(defn función-some-fn-4
  []
  (let [f (fn[x] (true? x))
        
        z (some-fn f)]

    (z true)))

(defn función-some-fn-5
  []
  (let [f (fn[x] (true? x))
        
        z (some-fn f)]

    (z [1 2])))

(defn función-some-fn-6
  []
  (let [f (fn[x] (true? x))
        
        z (some-fn f)]

    (z -54)))

(defn función-some-fn-7
  []
  (let [f (fn[x] (true? x))
        
        z (some-fn f)]

    (z \b)))

(defn función-some-fn-8
  []
  (let [f (fn [x] (true? x))

        z (some-fn f)]

    (z \a)))

(defn función-some-fn-9
  []
  (let [f (fn[x] (true? x))
        
        z (some-fn f)]

    (z "hola mundo")))

(defn función-some-fn-10
  []
  (let [f (fn[x] (true? x))
        
        z (some-fn f)]

    (z 8343)))

(defn función-some-fn-11
  []
  (let [f (fn[x] (true? x))
        
        z (some-fn f)]

    (z "e")))

(defn función-some-fn-12
  []
  (let [f (fn[x] (true? x))
        
        z (some-fn f)]

    (z -43)))

(defn función-some-fn-13
  []
  (let [f (fn[x] (true? x))
        
        z (some-fn f)]

    (z 243)))

(defn función-some-fn-14
  []
  (let [f (fn[x] (true? x))
        
        z (some-fn f)]

    (z 8/8)))

(defn función-some-fn-15
  []
  (let [f (fn[x] (true? x))
        
        z (some-fn f)]

    (z 8/9)))

(defn función-some-fn-16
  []
  (let [f (fn [x] (true? x))

        z (some-fn f)]

    (z -7/5)))

(defn función-some-fn-17
  []
  (let [f (fn [x] (true? x))

        z (some-fn f)]

    (z 83333)))

(defn función-some-fn-18
  []
  (let [f (fn [x] (true? x))

        z (some-fn f)]

    (z :a)))

(defn función-some-fn-19
  []
  (let [f (fn[x] (true? x))
        
        z (some-fn f)]

    (z :b)))

(defn función-some-fn-20
  []
  (let [f (fn [x] (true? x))

        z (some-fn f)]

    (z 12)))

(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)

